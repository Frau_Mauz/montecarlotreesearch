import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

enum Result {
    WIN(1),
    LOSS(0);

    private final int value;

    Result(int value) {
        this.value = value;
    }

    public int value() {
        return this.value;
    }
}

interface IState {
    /**
     * Contains the result if this state finishes the game in some way.
     * makes later request of 'state.result().isEmpty())' in simulation possible
     */
    Optional<Result> result();
    }

class Tree<S extends IState> {
    private int w = 0;          // number of wins
    private int n = 0;          // number of simulations
    private final int color;    // 0 = "white", 1 = "black"
    private final S state;    // value as generic type
    private final Function<S, S> transition;  // function for game, used as parameter in Tree constructor, anonymous function
    private final List<Tree<S>> children = new LinkedList<>();  //list of trees, one node is a tree, children itself also trees
    private final Tree<S> parent; // parent is a tree

    private static final double C = Math.sqrt(2);  // for exploration in UCT1-algorithm,

    private Tree(S root, Function<S, S> transition, Tree<S> parent) {
        this.color = parent == null ? 0 : (parent.color + 1) % 2;  //modulo: either 0 or 1 (we are black, computer is white)
        this.state = root;
        this.transition = transition;
        this.parent = parent;
    }

    /**
     * Creates a new Tree instance.
     * @param initial The initial state of the game
     * @param transition A function that, given some state `s` yields a new state `transition(s)`
     */
    public Tree(S initial, Function<S, S> transition) {
        this(initial, transition, null);
    }

    /**
     * Add a new subtree underneath this Tree.
     * For testing to create tree
     * @param state The subtree root's state
     * @return The created subtree
     */
    public Tree<S> add(S state) {
        var tree = new Tree<>(state, this.transition, this);
        this.children.add(tree);
        return tree;
    }

    /**
     * The state associated with this Tree.
     */
    public S state() {
        return this.state;
    }

    /**
     * Compute and do the next move and return the respective subtree.
     * Select with uct1-algorithm, expand randomly, simulate and get result and backprogate
     * Collextion.max ... !!!
     */
    public Tree<S> move() {
        var selectedNode = this.select();
        var expandedNode = this.expand(selectedNode);
        var result = this.simulate(expandedNode.state);
        this.backpropagate(expandedNode, result);

        //comparison of best move and returned
        return Collections.max(this.children, Comparator.comparing(v -> (double) v.w / v.n));
    }

    /**
     * find the best selection for a move with the uct1 algorithm and a
     * combination of exploration and exploitation
     * @return current Tree
     */
    private Tree<S> select() {
        var current = this;

        //while tree has children
        while (!current.children.isEmpty()) {
            final int N = current.n;
            current = Collections.max(current.children, Comparator.comparing(t -> {
                //known and good steps
                var exploitation = t.n > 0 ? (double) t.w / t.n : 0;
                //exploration of new steps
                var exploration = t.n > 0 ? Tree.C * Math.sqrt(Math.log(N) / t.n) : 0;  // t.n > 0  for n = root
                return exploitation + exploration;
            }));
        }
        return current;
    }
    /**
     * Expands the tree by one node
     * @param node
     * @return a tree after adding of node
     */
    private Tree<S> expand(Tree<S> node) {
        return node.add(this.transition.apply(node.state));
    }

    /**
     * simulation of a game until somebody wins or looses
     * @param state
     * @return state.result().get() as Result
     */
    private Result simulate(S state) {
        // TODO: introduce limit to prohibit infinite looping?
        while (state.result().isEmpty()) {
            state = this.transition.apply(state);
        }
        return state.result().get();
    }

    /**
     * backpropagate the number of wins and the amount of simulations back until the root is reached
     * @param node
     * @param result
     */
    private void backpropagate(Tree<S> node, Result result) {
        while (node != null) {
            // bitwise ~ NOT, bitwise ^ XOR, together NXOR
            // color -> white = 0, black = 1
            // result -> win = 1, loss = 0
            node.w += ~(result.value() ^ node.color);
            node.n++;
            node = node.parent;
        }
    }

    /**
     * Puts nodes and edges on Stack and outputs state
     * Line by line and separated by commas
     * The node that was last put on the stack is taken down first
     * @return sb.toString()
     */
    @Override
    public String toString() {
        var sb = new StringBuilder();
        var stack = new Stack<Tree<S>>();
        stack.push(this);
        while (!stack.empty()) {
            var node = stack.pop();
            var edges = node.children.stream().map(child -> {
                stack.push(child);
                return "(%s, %s)".formatted(node.state.toString(), child.state.toString());
            }).collect(Collectors.joining(", "));
            if (!edges.isEmpty()) {
                sb.append(edges).append("\r\n");
            }
        }
        return sb.toString();
    }
}

/**
 * A record containing state for our test sample game
 * record: Short version of constructor, new in Java 16
 */
record State(String key, int state) implements IState {
    public static State of(int key, int value) {
        return new State(String.valueOf(key), value);
    }

    /**
     * Optional: that can have a value or not, wrapper of Result
     * checks first if not empty, gets the Result if not empty
     * @return state as Optional
     */
    @Override
    public Optional<Result> result() {
        return state > 3 ? Optional.of(Result.WIN) : Optional.empty();  // dummy result determination
    }

    /**
     * Formatted string to be close to the given code of Victor Bretag
     * @return
     */
    @Override
    public String toString() {
        return "[%s, %d]".formatted(this.key, this.state);
    }
}

public class Main {
    public static void main(String[] args) {
        //Tree start state and lambda function with state parameter

        var tree = new Tree<>(State.of(0, 0), state -> { // given as function, that is used as transition with state -> ...
            // random node will be expanded from 3 possible nodes
            int expansionNodeIndex = (int) (Math.random() * 3) + 1; //random number: 1, 2 or 3
            return switch (expansionNodeIndex) {
                case 1 -> State.of(1, -2);  // if random 1 expand with key 1

                //key statt 1 : "(3_1_1_X)" - x = case

                case 2 -> State.of(2, -1);  // if random 2 expand with key 2
                case 3 -> State.of(3, 4); // if random 3 expand with key 3
                default -> throw new IllegalStateException("Invalid key: %d".formatted(expansionNodeIndex));
            };
        });

        // create mcts example tree, just needed for test cases and visualization
        var root_1 = tree.add(State.of(1, -2));
        tree.add(State.of(2, -1));
        var root_3 = tree.add(State.of(3, 4));

        root_1.add(State.of(1, 3));
        root_1.add(State.of(2, -2));
        var root_1_3 = root_1.add(State.of(3, -2));

        root_1_3.add(State.of(1, -3));
        root_1_3.add(State.of(2, -2));

        root_3.add(State.of(1, 4));
        root_3.add(State.of(2, 5));

        System.out.println(tree); // prints edges of nodes

        var root = tree;

        // as long as no result make a move consisting of all 4 phases of the mcts
        while (tree.state().result().isEmpty()) {
            tree = tree.move();
        }

        System.out.println(tree.state().result().get()); // prints win or loss
        System.out.println(root); // print tree from real root forward
    }
}
