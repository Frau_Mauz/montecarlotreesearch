# Monte-Carlo-Tree-Search

This is the repository for the research project as part of the Master´s 'Applied Computer Science' at HTW Berlin 2021.
Monte-Carlo-Tree-Search is explained and implemented in this little sub-project.

This README is created to share and publish my research project dealing with the GriScha project and MCTS. 



## WHO 
```
Jennifer Vormann | Applied Computer Science (M.Sc.) | 2nd semester | HTW Berlin |
```


## Installation | Instructions  | General
- To run this project please install Java 16 and activate the JDK in IntelliJ (-> needed for records & switch expressions)
- clone this project via ssl or https
- read the documentation

## 
![coding](https://paperswithcode.com/media/methods/Screen_Shot_2020-06-29_at_9.36.32_PM_Vc3hZmF.png)

Source: https://paperswithcode.com/method/monte-carlo-tree-search 



